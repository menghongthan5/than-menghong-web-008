
import React, { Component } from 'react';
import { Container, Row } from 'react-bootstrap';
import MyForm from './components/MyForm';
import MyTable from './components/MyTable';
import NavMenu from "./components/NavMenu";
import AddTb from "./components/AddTb";
class App2 extends Component {
    render() {
        return (
            <Container>
                <NavMenu />
                <Row>
                    <MyForm />
                </Row>
                <AddTb />
            </Container>
        );
    }
}

export default App2;
