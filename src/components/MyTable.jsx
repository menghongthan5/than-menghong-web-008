import React from "react";
import { Table, Button } from "react-bootstrap";

export default function MyTable({ foods, onClear }) {
  console.log("Props on func:", foods);
  //let {foods, test} = {...props}

  let temp = foods.filter(item=>{
      return item.qty > 0
  })

  return (
    <>
    
    <Button onClick={onClear} variant="warning">Clear all</Button>
    <h2>{temp.length} foods</h2>
    <Table striped bordered hover>
  <thead>
    <tr>
      <th>#</th>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Username</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td>Mark</td>
      <td>Otto</td>
      <td>@mdo</td>
    </tr>
    <tr>
      <td>2</td>
      <td>Jacob</td>
      <td>Thornton</td>
      <td>@fat</td>
    </tr>
    <tr>
      <td>3</td>
      <td colSpan="2">Larry the Bird</td>
      <td>@twitter</td>
    </tr>
  </tbody>
</Table>
    </>
  );
}
