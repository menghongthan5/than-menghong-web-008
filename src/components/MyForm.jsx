import React, { Component } from "react";
import { Form, Button, Image } from "react-bootstrap";

class MyForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      emailErr: "",
      passwordErr: "",
      isSignin: false,
      image: null,
      imageURL: null,
      gender: "female"
    };
  }

  onHandleText = (e) => {
    console.log("e:", e.target);
    //let name = "email"
    this.setState(
      {
        [e.target.name]: e.target.value,
      },
      () => {
        console.log(this.state);

        if (e.target.name === "email") {
          //apple1212@#@gmail.com
          let pattern = /^\S+@\S+\.[a-z]{3}$/g;
          let result = pattern.test(this.state.email.trim());

          if (result) {
            this.setState({
              emailErr: "",
            });
          } else if (this.state.email === "") {
            this.setState({
              emailErr: "Email cannot be empty!",
            });
          } else {
            this.setState({
              emailErr: "Email is invalid!",
            });
          }
        }

        if (e.target.name === "password") {
          let pattern2 =
            /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[_\W])([a-zA-Z0-9_\W]{8,})$/g;
          let result2 = pattern2.test(this.state.password);

          if (result2) {
            this.setState({
              passwordErr: "",
            });
          } else if (this.state.password === "") {
            this.setState({
              passwordErr: "Password cannot be empty!",
            });
          } else {
            this.setState({
              passwordErr: "Password is invalid!",
            });
          }
        }
      }
    );
  };

  validateBtn = () => {
    if (
      this.state.emailErr === "" &&
      this.state.passwordErr === "" &&
      this.state.email !== "" &&
      this.state.password !== ""
    ) {
      return false;
    } else {
      return true;
    }
  };

  onUploadFile = (e) => {
    console.log("Image: ", e.target.files[0]);
    this.setState({
      imageURL: URL.createObjectURL(e.target.files[0]),
    });
  };


  onTest = () => {
    let arr = ["banana", "asdfas", "adsfas", "banana"]

    //   let temp = arr.map(item=>{
    //       return item
    //   })

    // let temp = arr.filter(item=>{
    //     return item === "banana"
    // })

    let temp = arr.find(item => {
      return item === "banana"
    })

    // let temp = arr.some(item=>{
    //     return item === "banana"
    // })

    // let temp = arr.every(item=>{
    //     return item === "banana"
    // })


    console.log("TEMP:", temp)
    //filter
    //map
    //find
    //some
    //every
  }

  render() {
    return (
      <Form>
          <Image
            width="200px"
            height="200px"
            src="./img/pizza.jpg"
            roundedCircle
          />

        {/* <input
          onChange={this.onUploadFile}
          style={{ display: "none" }}
          id="myfile"
          type="file"
        /> */}
        <h1>Create Account</h1>
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Username</Form.Label>
          <Form.Control
            name="username"
            onChange={this.onHandleText}
            type="email"
            placeholder="username"
          />
          <Form.Text className="text-muted">
            <p style={{ color: "red" }}>{this.state.passwordErr}</p>
          </Form.Text>
        </Form.Group>
        <Form.Label>Gender</Form.Label>
        <br/>
        <Form.Check
          custom
          inline
          label="Male"
          type="radio"
          id="male"
          name="gender"
          defaultChecked={this.state.gender === "male" ? true : false}
        />
        <Form.Check
          custom
          inline
          label="Female"
          type="radio"
          id="female"
          name="gender"
          defaultChecked={this.state.gender === "female" ? true : false}
        />
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Email</Form.Label>
          <Form.Control
            name="email"
            onChange={this.onHandleText}
            type="email"
            placeholder="email"
          />
          <Form.Text className="text-muted">
            <p style={{ color: "red" }}>{this.state.emailErr}</p>
          </Form.Text>
        </Form.Group>

        <Form.Group controlId="formBasicEmail">
          <Form.Label>Password</Form.Label>
          <Form.Control
            name="password"
            onChange={this.onHandleText}
            type="email"
            placeholder="password"
          />
          <Form.Text className="text-muted">
            <p style={{ color: "red" }}>{this.state.passwordErr}</p>
          </Form.Text>
        </Form.Group>

        <br />

        <Button disabled={this.validateBtn()} variant="primary" type="button">
          Save
        </Button>
      </Form>
    );
  }
}

export default MyForm;
